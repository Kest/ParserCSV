import org.junit.*;

import java.util.HashMap;

public class PersonsMapTest extends Assert {

    Parser parser;

    PersonsMap personsMap = new PersonsMap();

    static HashMap<String, Person> personsTest = new HashMap<>();

    Person personTest = Person.createPerson("Костя", 22, "+79117449622", "Санкт-Петербург");

    @BeforeClass
    public static void parserStart(){
        personsTest.put("Костя", Person.createPerson("Костя", 22, "+79117449622", "Санкт-Петербург"));
        personsTest.put("Юля", Person.createPerson("Юля", 23, "+79117449622", "Кишинев"));
        personsTest.put("Роман", Person.createPerson("Роман", 27, "+79117449622", "Минск"));
        personsTest.put("Никита", Person.createPerson("Никита", 25, "+79117449622", "Санкт-Петербург"));

    }

    @Before
    public void beforeTest(){
        parser = new Parser("src/main/resources/persons.csv");
    }

    @Test
    public void positiveRecordPersonsTest (){
        personsMap.writePersons(parser);
        assertEquals(personsTest, personsMap.getPersons());
    }

    @Test
    public void positiveGetPersonTest (){
        personsMap.writePersons(parser);
        assertEquals(personTest, personsMap.getPerson("Костя"));
    }

    @Test
    public void positiveDeletePerson (){
        personsMap.writePersons(parser);
        int k = personsMap.getPersons().size();
        assertTrue(personsMap.deletePerson("Костя"));
        k--;
        assertEquals(k, personsMap.getPersons().size());
    }

    @Test
    public void negativeRecordPersonsTest (){
        personsMap.writePersons(parser);
        assertNotEquals(3, personsMap.getPersons().size());
    }

    @Test
    public void negativeGetPersonTest (){
        personsMap.writePersons(parser);
        assertNull(personsMap.getPerson("Коля"));
    }

    @Test
    public void negativeDeletePerson (){
        personsMap.writePersons(parser);
        assertFalse(personsMap.deletePerson("Кост"));
    }

    @After
    public void afterTest(){
        parser.closeConnection();
    }

    @AfterClass
    public static void parserClose(){
        personsTest.clear();
    }
}
