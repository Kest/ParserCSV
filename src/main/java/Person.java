import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person {

    private String name;
    private int age;
    private String telephone;
    private String address;

    private Person(String name, int age, String telephone, String address) {
        this.name = name;
        this.age = age;
        this.telephone = telephone;
        this.address = address;
    }

    public static Person createPerson(String name, int age, String telephone, String address){
        if (validateAge(age)){
            if (validateTelephone(telephone))
                return new Person(name, age, telephone, address);
            else {
                System.out.println("Данные пользователя " + name + " не корректны (telephone)");
                return null;
            }
        } else {
            System.out.println("Данные пользователя " + name + " не корректны (age)");
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    private static boolean validateAge(int age){
        return age > 0;
    }

    private static boolean validateTelephone(String telephone){
        Pattern p = Pattern.compile("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$");
        Matcher m = p.matcher(telephone);
        return m.matches();
    }

    @Override
    public String toString() {
        return name + " | " + age + " | " + telephone + " | " + address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (!name.equals(person.name)) return false;
        if (telephone != null ? !telephone.equals(person.telephone) : person.telephone != null) return false;
        return address != null ? address.equals(person.address) : person.address == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age;
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
