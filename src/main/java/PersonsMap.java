import java.util.HashMap;

public class PersonsMap {

    private HashMap<String, Person> persons = new HashMap<>();

    private Parser parser;

    public void writePersons(Parser parser) {
        this.parser = parser;
        persons = parser.parserPersons();
    }

    public Person getPerson(String name){
        try {
            return persons.get(name);
        } catch (NullPointerException e){
            return null;
        }
    }

    public boolean deletePerson(String name){
        return persons.remove(name, persons.get(name));
    }

    public HashMap<String, Person> getPersons(){
        return persons;
    }
}
