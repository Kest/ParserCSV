public enum PersonParameters {

    NAME (0), AGE (1), TELEPHONE (2), ADDRESS (3);

    private int position;

    private PersonParameters(int position){
        this.position = position;
    }

    public int getPosition(){
        return position;
    }
}
