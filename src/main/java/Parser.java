import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Parser {

    private HashMap<String, Person> persons = new HashMap<>();

    private FileInputStream fileInputStream;

    private Scanner scanner;

    //"src/main/resources/persons.csv"
    private String path;
    private String text;

    private String[] split;

    private Person person;

    public Parser(String path){
        this.path = path;
        createConnection(path);
    }

    private void createConnection(String path){
        try {
            fileInputStream = new FileInputStream(new File(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Person createPerson(String line){
        split = line.split("\\|");
        return Person.createPerson(split[PersonParameters.NAME.getPosition()],
                Integer.parseInt(split[PersonParameters.AGE.getPosition()]),
                split[PersonParameters.TELEPHONE.getPosition()],
                split[PersonParameters.ADDRESS.getPosition()]);
    }

    public HashMap<String, Person> parserPersons(){
        scanner = new Scanner(fileInputStream);
        while (scanner.hasNextLine()){
            text = scanner.nextLine();
            person = createPerson(text);
            persons.put(person.getName(), person);
        }
        return persons;
    }

    public void closeConnection() {
        try {
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
